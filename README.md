# ibms-dahua

#### 介绍

​		项目主要用来对接大华的人脸门禁(闸机),可以实现门禁卡的管理及对应的人脸的管理,同时可以设置门禁卡的类别以及有效时间段,母卡等.可以在此基础上实现各种业务需求,例如人脸快递柜,基于人脸识别的会员卡系统等.

ps:项目不提供对门禁设备的直接控制接口,但可以对门禁卡的有效时间进行管理进而判断对应用户是否有进出权限.需要配合大华的人脸门禁或人脸闸机使用(项目不含任何与人脸识别的相关算法,所有操作基于大华的设备,请无相关设备的朋友谨慎下载)

<p align='center'> <img src='doc/images/image-20210808172636292.png' title='images' ></img> </p>

1.项目依赖

>项目集成自大华官网对接的sdk
>
>https://www.dahuatech.com/service/downloadlists/836.html
>
><p align='center'> <img src='doc/images/image-20210730103619073.png' title='images' ></img> </p>
>
>> windows64位sdk下载地址
>>
>> http://7viih2.com1.z0.glb.clouddn.com/2.8.01.01.01898/20210625/936699_General_NetSDK_ChnEng_JAVA_Win64_IS_V3.055.0000000.0.R.210602.zip
>>
>> linux64位sdk下载地址
>>
>> http://7viih2.com1.z0.glb.clouddn.com/2.8.01.01.01894/20210625/936691_General_NetSDK_ChnEng_JAVA_Linux64_IS_V3.055.0000000.0.R.210602.zip

2.项目运行环境

项目同时支持运行于windows平台和linux平台

>主要代码来自window版本的sdk(但是删减了其中的大部分示例代码,只保留了和人脸门禁(闸机)相关的代码以及一些基础的代码),同时将linux版本的需要的库文件放入了软件目录,部署在linux服务器上时需要将libs/linux64文件夹下的库文件拷贝至服务器的/tmp目录下

3.软件功能

门禁卡的增删改查,出入记录查询,门禁卡对应的人脸照片的上修改等

#### 软件架构
软件架构说明

springboot+maven


#### 安装教程

1. 配置设备信息(设备登录信息)

   项目尚未接入数据库,故人脸识别设备的信息需要在DeviceUtils中进行修改,生产最好直接将设备信息录入自建的数据库中

   <p align='center'> <img src='doc/images/image-20210808173106602.png' title='images' ></img> </p>

2. 项目打包同一般的springboot项目,不熟悉如何打包的请自行百度

   

3. 部署在linux平台时需要将项目libs/linux64目录下的库文件拷贝至服务器的/tmp目录下

<p align='center'> <img src='doc/images/image-20210808173558701.png' title='images' style="height:400px" ></img> </p>

<p align='center'> <img src='doc/images/image-20210808173711157.png' title='images' style="height:400px" ></img> </p>



#### 使用说明

PS:调试项目时需要先执行mvn install 以在classes文件夹中生成文件目录（否则可能会获取不到url资源）
接口文档请求地址:http://ip:端口/doc.html
​	

1. 获取卡列表

   <p align='center'> <img src='doc/images/image-20210808181459245.png' title='images' ></img> </p>

2. 添加卡

   <p align='center'> <img src='doc/images/image-20210808172643243.png' title='images'></img> </p>

3. 修改卡

   <img src='doc/images/image-20210808172704657.png' title='images'></img> 

4. 删除卡  

   <p align='center'> <img src='doc/images/image-20210808172730714.png' title='images'></img> </p>

5. 修改人脸照片

   <p align='center'> <img src='doc/images/image-20210808172759039.png' title='images'></img> </p>

6.  获取出入记录

    <p align='center'> <img src='doc/images/image-20210808172821623.png' title='images'></img> </p>

    

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
