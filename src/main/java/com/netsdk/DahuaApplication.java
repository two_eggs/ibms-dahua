package com.netsdk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 主启动
 *
 * @author Yan Xu
 * @version 1.0
 * @date 2021/8/6
 * Copyright © goodits
 */
@SpringBootApplication
public class DahuaApplication {
    public static void main(String[] args) {
        SpringApplication.run(DahuaApplication.class,args);
    }
}
