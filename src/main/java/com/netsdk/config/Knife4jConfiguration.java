package com.netsdk.config;

import com.github.xiaoymin.knife4j.spring.annotations.EnableKnife4j;
import com.google.common.base.Predicates;
import springfox.documentation.service.Contact;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * 接口文档配置
 * @author Yan Xu
 * @version 1.0
 * @date 2021/8/7
  * Copyright © goodits
 */
@Configuration
@EnableSwagger2
@EnableKnife4j
public class Knife4jConfiguration {


    private final ServerProperties serverProperties;
    private final SysInfoProperties sysInfoProperties;

    public Knife4jConfiguration(ServerProperties serverProperties, SysInfoProperties sysInfoProperties) {
        this.serverProperties = serverProperties;
        this.sysInfoProperties = sysInfoProperties;
    }

    private ApiInfo apiInfo() {
        String serviceUrl = "http://localhost:" + serverProperties.getPort();
        return new ApiInfoBuilder()
                .title(sysInfoProperties.getChineseName()+"后台接口文档")
                .description(sysInfoProperties.getDescription())
                .termsOfServiceUrl(serviceUrl)
                .version(sysInfoProperties.getVersion())
                .contact(new Contact(sysInfoProperties.getPic(), null, "2366528143@qq.com"))
                .build();
    }

    @Bean(value = "defaultApi2")
    public Docket defaultApi2() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                //分组名称
                .groupName("defaultApi2")
                .select()
                //这里指定Controller扫描包路径
                //.apis(RequestHandlerSelectors.basePackage("com.goodits.controller"))
                // 对所有api进行监控
                .apis(RequestHandlerSelectors.any())
                //错误路径不监控
                .paths(Predicates.not(PathSelectors.regex("/error.*")))
                //服务质量检测路径不监控
                .paths(Predicates.not(PathSelectors.regex("/actuator.*")))
                .paths(PathSelectors.any())
                .build();
    }
}