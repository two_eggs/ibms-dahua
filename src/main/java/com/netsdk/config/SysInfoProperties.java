package com.netsdk.config;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
// 注册为组件
@EnableConfigurationProperties
// 启用配置自动注入功能
@ConfigurationProperties(prefix = "project")
//指定类对应的配置项前缀
@ApiModel(description = "系统描述")
public class SysInfoProperties {
    @ApiModelProperty(value = "应用中文名")
    private String chineseName;
    @ApiModelProperty(value = "应用描述")
    private String description;
    @ApiModelProperty(value = "版本")
    private String version;
    @ApiModelProperty(value = "开发")
    private String pic;
}