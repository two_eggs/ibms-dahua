package com.netsdk.controller;

import com.netsdk.bean.AccessRecord;
import com.netsdk.bean.CardDto;
import com.netsdk.bean.Result;
import com.netsdk.lib.ToolKits;
import com.netsdk.service.CardService;
import com.netsdk.utils.AccessRecordUtils;
import com.netsdk.utils.FileUploadUtils;
import com.netsdk.utils.ResultUtil;
import com.sun.jna.Memory;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * 卡管理控制器
 *
 * @author Yan Xu
 * @version 1.0
 * @date 2021/8/6
 * Copyright © goodits
 */
@RestController
@RequestMapping("/card")
@Api(tags = "卡管理")
@Valid
public class CardController {

    @Resource
    private CardService cardService;
    @Resource
    private FileUploadUtils fileUploadUtils;
    @Resource
    private AccessRecordUtils accessRecordUtils;

    @PostMapping("/getList")
    @ApiOperation("获取卡列表")
    public Result<List<CardDto>> getList(@ApiParam(value = "设备唯一id", required = true) @NotBlank String deviceNo, @ApiParam("卡号") String cardNo) {
        List<CardDto> list = cardService.getList(deviceNo, cardNo);
        return ResultUtil.data(list);
    }

    @PostMapping("/add")
    @ApiOperation("添加卡")
    public Result add(String deviceNo,
                      CardDto cardDto,
                      HttpServletRequest request) throws IOException {
        Result<String> uploadResult = fileUploadUtils.upload(cardDto.getFace());
        if (Boolean.FALSE.equals(uploadResult.isSuccess())) {
            return ResultUtil.error("文件上传失败");
        }
        String picPath = uploadResult.getResult();
        Memory memory = ToolKits.readPictureFile(picPath);
        //先发送请求到设备
        Result add = cardService.add(deviceNo, cardDto, memory);
        //删除文件
        fileUploadUtils.delete(picPath);
        return add;
    }

    @PostMapping("/update")
    @ApiOperation("修改卡")
    public Result update(String deviceNo,
                         CardDto cardDto,
                         HttpServletRequest request) throws IOException {
        Result<String> uploadResult = fileUploadUtils.upload(cardDto.getFace());
        if (Boolean.FALSE.equals(uploadResult.isSuccess())) {
            return ResultUtil.error("文件上传失败");
        }
        String picPath = uploadResult.getResult();
        Memory memory = ToolKits.readPictureFile(picPath);
        //先发送请求到设备
        Result update = cardService.update(deviceNo, cardDto, memory);
        //再删除文件
        fileUploadUtils.delete(picPath);
        return update;
    }

    @PostMapping("/updateFace")
    @ApiOperation("修改人脸照片")
    public Result updateFace(String deviceNo,
                             String userId,
                             MultipartFile face) throws IOException {
        Result<String> uploadResult = fileUploadUtils.upload(face);
        if (Boolean.FALSE.equals(uploadResult.isSuccess())) {
            return ResultUtil.error("文件上传失败");
        }
        String picPath = uploadResult.getResult();
        Memory memory = ToolKits.readPictureFile(picPath);
        //先发送请求到设备
        Result update = cardService.updateFace(deviceNo, userId, memory);
        //再删除文件
        fileUploadUtils.delete(picPath);
        return update;
    }

    @PostMapping("/delete")
    @ApiOperation("删除卡")
    public Result delete(@ApiParam(value = "设备唯一id", required = true) @NotBlank String deviceNo,
                         @ApiParam(value = "记录集编号", required = true) @NotBlank String recordNo,
                         @ApiParam(value = "用户id", required = true) @NotBlank String userId) {
        return cardService.delete(deviceNo, recordNo, userId);
    }

    @PostMapping("/getAccessRecords")
    @ApiOperation("获取出入记录列表")
    public Result<List<AccessRecord>> getAccessRecords(@ApiParam(value = "设备唯一id", required = true) @NotBlank String deviceNo,
                                                       @ApiParam(value = "卡号") String cardNo,
                                                       @ApiParam(value = "起始时间") String startTime,
                                                       @ApiParam(value = "结束时间") String endTime) throws UnsupportedEncodingException {
        return accessRecordUtils.findRecords(deviceNo, cardNo, startTime, endTime);
    }

}
