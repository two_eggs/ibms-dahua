package com.netsdk.controller;


import com.netsdk.bean.Result;
import com.netsdk.utils.FileUploadUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;


@RestController("/file")
@Slf4j
@Api(tags = "文件上传")
public class UploadController {

    @Resource
    private FileUploadUtils fileUploadUtils;

    /**
     * 上传多个文件
     *
     * @param file 上传文件集合
     * @return
     */
    @PostMapping(value = "/upload")
    @ApiOperation("上传文件")
    public Result<String> upload(MultipartFile file) throws IOException {
        return fileUploadUtils.upload(file);
    }

}
