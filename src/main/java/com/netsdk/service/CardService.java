package com.netsdk.service;

import com.netsdk.bean.CardDto;
import com.netsdk.bean.Result;
import com.sun.jna.Memory;
import io.swagger.annotations.ApiParam;
import org.springframework.web.multipart.MultipartFile;

import javax.smartcardio.Card;
import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * 卡操作服务
 *
 * @author Yan Xu
 * @version 1.0
 * @date 2021/8/6
 * Copyright © goodits
 */
public interface CardService {
    List<CardDto> getList(String deviceNo, String cardNo);

    Result add(String deviceNo, CardDto cardDto, Memory memory);

    Result update(String deviceNo, CardDto cardDto, Memory memory);

    Result updateFace(String deviceNo,String userId, Memory memory);

    Result delete(String deviceNo, String recordNo, String userId);
}
