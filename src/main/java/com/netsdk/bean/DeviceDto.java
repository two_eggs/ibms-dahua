package com.netsdk.bean;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;

/**
 * 人脸闸机设备
 *
 * @author Yan Xu
 * @version 1.0
 * @date 2021/8/6
 * Copyright © goodits
 */
@ApiModel("人脸闸机设备")
@lombok.Data
@AllArgsConstructor
public class DeviceDto {
    @ApiModelProperty("设备唯一id")
    private String id;
    @ApiModelProperty("IP地址")
    private String m_strIp;
    @ApiModelProperty("端口号")
    private Integer m_nPort;
    @ApiModelProperty("用户名")
    private String m_strUser;
    @ApiModelProperty("密码")
    private String m_strPassword;
}
