package com.netsdk.bean;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.web.multipart.MultipartFile;

/**
 * 门禁卡对象
 *
 * @author Yan Xu
 * @version 1.0
 * @date 2021/8/6
 * Copyright © goodits
 */
@lombok.Data
@ApiModel("门禁卡")
public class CardDto {

    @ApiModelProperty(value = "序号",hidden = true)
    private String serialNumber;

    @ApiModelProperty("卡号")
    private String cardNo;

    @ApiModelProperty("用户id")
    private String userId;

    @ApiModelProperty("卡名")
    private String cardName;

    @ApiModelProperty("卡密码")
    private String cardPasswd;

    @ApiModelProperty("卡状态[-1:未知|0:正常|1:挂失|2:注销|3:冻结|4:欠费|5:逾期|6:预欠费]")
    private Integer cardStatus;

    @ApiModelProperty(value = "卡状态名称",hidden = true)
    private String cardStatusName;

    @ApiModelProperty("卡类型[-1:未知|0:一般卡|1:VIP卡|2:来宾卡|3:巡逻卡|4:黑名单卡|5:胁迫卡|6:巡检卡|7:母卡]")
    private Integer cardType;

    @ApiModelProperty(value = "卡类型名称",hidden = true)
    private String cardTypeName;

    @ApiModelProperty("使用次数，仅当卡类型为来宾卡时有效")
    private String useTimes;

    @ApiModelProperty("是否首卡[0:不是|1:是]")
    private Integer firstEnter;

    @ApiModelProperty("是否有效[0:不是|1:是]")
    private Integer enable;

    @ApiModelProperty("有效期开始时间")
    private String startTime;

    @ApiModelProperty("有效期结束时间")
    private String endTime;

    @ApiModelProperty("记录集编号， 修改、删除卡信息必须填写")
    private Integer recordNo;

    @ApiModelProperty("人脸照片")
    private MultipartFile face;
}
