package com.netsdk.bean;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("出入记录")
@lombok.Data
public class AccessRecord {
    @ApiModelProperty("记录集编号,只读")
    public int recordNo;                                 // 记录集编号,只读
    @ApiModelProperty("卡号")
    public String cardNo;// 卡号
    @ApiModelProperty("卡名称")
    public String cardName;// 卡名称
    @ApiModelProperty("刷卡时间")
    public String stuTime;                                // 刷卡时间
    @ApiModelProperty("刷卡结果")
    public int bStatus;                                // 刷卡结果,TRUE表示成功,FALSE表示失败
    @ApiModelProperty("开门方式")
    public int emMethod;                                // 开门方式 NET_ACCESS_DOOROPEN_METHOD
    @ApiModelProperty("门号")
    public int door;                                  // 门号,即CFG_CMD_ACCESS_EVENT配置CFG_ACCESS_EVENT_INFO的数组下标
    @ApiModelProperty("用户ID")
    public String userId; // 用户ID
    @ApiModelProperty("开锁抓拍上传的FTP地址")
    public String snapFtpUrl;        // 开锁抓拍上传的FTP地址
    @ApiModelProperty("读卡器ID")
    public String readerID;// 读卡器ID													// 开门并上传抓拍照片,在记录集记录存储地址,成功才有
    @ApiModelProperty("卡类型")
    public int cardType;                            // 卡类型 NET_ACCESSCTLCARD_TYPE
    @ApiModelProperty("开门失败的原因")
    public int errorCode;                            // 开门失败的原因,仅在bStatus为FALSE时有效
}