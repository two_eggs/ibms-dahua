package com.netsdk.utils;

import cn.hutool.core.util.ObjectUtil;
import com.netsdk.bean.DeviceDto;
import com.netsdk.demo.module.LoginModule;
import lombok.extern.slf4j.Slf4j;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 设备工具类
 * (暂时使用静态数据，实际使用时需要使用数据库中的数据)
 *
 * @author Yan Xu
 * @version 1.0
 * @date 2021/8/6
 * Copyright © goodits
 */
@Slf4j
public class DeviceUtils {

    private static final Map<String, DeviceDto> DEVICE_DTO_MAP = new LinkedHashMap<>(16);


    static {
        DeviceDto deviceDto = new DeviceDto("1", "172.16.4.222", 37777, "admin", "123456abc");
        DEVICE_DTO_MAP.put(deviceDto.getId(), deviceDto);
        DeviceDto deviceDto1 = new DeviceDto("2", "192.168.90.201", 37777, "admin", "gt199300");
        DEVICE_DTO_MAP.put(deviceDto1.getId(), deviceDto1);
        DeviceDto deviceDto2 = new DeviceDto("3", "192.168.90.202", 37777, "admin", "gt199300");
        DEVICE_DTO_MAP.put(deviceDto2.getId(), deviceDto2);
        DeviceDto deviceDto3 = new DeviceDto("4", "192.168.90.203", 37777, "admin", "gt199300");
        DEVICE_DTO_MAP.put(deviceDto3.getId(), deviceDto3);
        DeviceDto deviceDto4 = new DeviceDto("5", "192.168.90.204", 37777, "admin", "gt199300");
        DEVICE_DTO_MAP.put(deviceDto4.getId(), deviceDto4);
    }

    public static boolean login(String deviceNo) {

        DeviceDto deviceDto = DEVICE_DTO_MAP.get(deviceNo);
        if (ObjectUtil.isEmpty(deviceDto)) {
            log.warn("不存在设备：{}", deviceDto);
            return false;
        }
        return LoginModule.login(deviceDto.getM_strIp(), deviceDto.getM_nPort(), deviceDto.getM_strUser(), deviceDto.getM_strPassword());
    }

    public static void logout() {
        LoginModule.logout();
    }
}
