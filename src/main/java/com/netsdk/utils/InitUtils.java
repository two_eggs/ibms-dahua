package com.netsdk.utils;

import com.netsdk.common.Res;
import com.netsdk.demo.module.LoginModule;
import com.netsdk.lib.NetSDKLib;
import com.sun.jna.Pointer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.*;

/**
 * 初始化
 *
 * @author Yan Xu
 * @version 1.0
 * @date 2021/8/6
 * Copyright © goodits
 */
@Component
@Slf4j
public class InitUtils {

    /**
     * // 设备断线回调: 通过 CLIENT_Init 设置该回调函数，当设备出现断线时，SDK会调用该函数
     */
    private static class DisConnect implements NetSDKLib.fDisConnect {
        @Override
        public void invoke(NetSDKLib.LLong m_hLoginHandle, String pchDVRIP, int nDVRPort, Pointer dwUser) {
            log.info("Device[{}}] Port[{}}] DisConnect!\n", pchDVRIP, nDVRPort);
            // 断线提示
            SwingUtilities.invokeLater(() -> log.info(Res.string().getFaceRecognition() + " : " + Res.string().getDisConnectReconnecting()));
        }
    }

    /**
     * // 网络连接恢复，设备重连成功回调
     * // 通过 CLIENT_SetAutoReconnect 设置该回调函数，当已断线的设备重连成功时，SDK会调用该函数
     */
    private static class HaveReConnect implements NetSDKLib.fHaveReConnect {
        @Override
        public void invoke(NetSDKLib.LLong m_hLoginHandle, String pchDVRIP, int nDVRPort, Pointer dwUser) {
            log.info("ReConnect Device[{}] Port[{}]\n", pchDVRIP, nDVRPort);
            // 重连提示
            SwingUtilities.invokeLater(() -> log.info(Res.string().getFaceRecognition() + " : " + Res.string().getOnline()));
        }
    }

    /**
     * 设备断线通知回调
     */
    private static final DisConnect DIS_CONNECT = new DisConnect();

    /**
     * 网络连接恢复
     */
    private static final HaveReConnect HAVE_RE_CONNECT = new HaveReConnect();


    @PostConstruct
    void init() {
       LoginModule.init(null, null);
    }
}
